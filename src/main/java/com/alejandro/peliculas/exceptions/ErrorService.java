package com.alejandro.peliculas.exceptions;

public class ErrorService extends Exception {
	
	public ErrorService(String msn) {
		
		super(msn);
	}

}
