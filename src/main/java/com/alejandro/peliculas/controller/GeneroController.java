package com.alejandro.peliculas.controller;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alejandro.peliculas.models.entity.Genero;
import com.alejandro.peliculas.models.service.IGeneroService;

@RestController
@RequestMapping(value = "/genero")
public class GeneroController {

	@Autowired
	IGeneroService generoService;

	@PostMapping()
	public ResponseEntity<Genero> addGenero(@RequestBody Genero genero) {
		return new ResponseEntity<Genero>(generoService.createGenero(genero), HttpStatus.CREATED);
	}
	
	@PutMapping(value= "/genero/{id}")
	public ResponseEntity<Genero> updateGenero(@RequestBody Genero genero, @PathVariable("id") Long idGenero) {
		Genero generoUpdate = generoService.updateGenero(idGenero, genero);
		return new ResponseEntity<>(generoUpdate, HttpStatus.OK);
	}
	
	@DeleteMapping(value = "/genero/ {id}")
	public void eliminarGenero(@PathVariable("id") Long idGenero) {
		 generoService.eliminarGenero(idGenero);
		
	}
	
	
	@GetMapping
	public ResponseEntity<List<Genero>> findAll(){
		List<Genero> buscar = generoService.findAll();
		
		
		return new ResponseEntity<List<Genero>>(buscar,HttpStatus.OK);
		
	}

}
