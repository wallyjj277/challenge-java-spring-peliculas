package com.alejandro.peliculas.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.alejandro.peliculas.exceptions.ErrorService;
import com.alejandro.peliculas.models.entity.Usuario;
import com.alejandro.peliculas.models.service.UsuarioService;

@Controller
@RequestMapping("/usuario")
public class UsuarioController {

	@Autowired
	private UsuarioService usuarioServicio;

	// @Autowired
	// private IGeneroDao iGeneroDao; ----> la relacion tendria que ser con
	// peliculas

	@PreAuthorize("hasanyrole('ROLE_USUARIO_REGISTRADO')")
	@GetMapping("/editar-perfil")
	public String editarPerfil(HttpSession session, @RequestParam String id, ModelMap model) {
		Usuario login = (Usuario) session.getAttribute("usuariosession");
		if (login == null || !login.getId().equals(id)) {
			return "redirect:/inicio";
		}
		try {
			Usuario usuario = usuarioServicio.buscarPorId(id);
			model.addAttribute("perfil", usuario);

		} catch (ErrorService e) {
			model.addAttribute("error", e.getMessage());
		}
		return "perfil.html";

	}

	@PreAuthorize("hasAnyRole('ROLE_USUARIO_REGISTRADO')")
	@PostMapping("/actualizar-perfil")
	public String registrar(ModelMap modelo, HttpSession session, MultipartFile archivo, @RequestParam String id,
			@RequestParam String nombre, @RequestParam String apellido, @RequestParam String mail,
			@RequestParam String clave1, @RequestParam String clave2) {
		Usuario usuario = null;

		try {
			Usuario login = (Usuario) session.getAttribute("usuariosession");
			if (login == null || !login.getId().equals(id)) {
				return "redirect:/inicio";
			}

			usuario = usuarioServicio.buscarPorId(id);
			usuarioServicio.modificar(archivo, id, nombre, apellido, mail, clave2, clave2);
			session.setAttribute("usuariosession", usuario);

			return "redirect:/inicio";

		} catch (ErrorService ex) {
			modelo.put("error", ex.getMessage());
			return "perfil.html";

		}
	}

}
