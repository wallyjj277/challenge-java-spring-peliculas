package com.alejandro.peliculas.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alejandro.peliculas.models.entity.Personaje;
import com.alejandro.peliculas.models.service.IPersonajeService;

@RestController
@RequestMapping(value ="/personaje")
public class PersonajeController {
	
	@Autowired
	private IPersonajeService personajeService;
	
	@GetMapping
	public ResponseEntity<Object> findAll(){
		System.out.println("get mapping findALL");
		List<Personaje> personajes = personajeService.findAll();
		return new ResponseEntity<>(personajes,HttpStatus.OK);
		
	}

}
