package com.alejandro.peliculas.models.service;

import java.util.List;

import com.alejandro.peliculas.models.entity.PeliculaSerie;

public interface IPeliculaSerieService {

	List<PeliculaSerie> findAll();

	PeliculaSerie createPeliculaSerie(PeliculaSerie peliculaSerie);

	PeliculaSerie updatePeliculaSerie(Long idPeliculaSerie, PeliculaSerie peliculaSerie);

	PeliculaSerie eliminarPeliculaSerie(PeliculaSerie peliculaSerie);

}
