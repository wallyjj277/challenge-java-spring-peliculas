package com.alejandro.peliculas.models.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;

import com.alejandro.peliculas.exceptions.ErrorService;
import com.alejandro.peliculas.models.dao.IUsuarioDao;
import com.alejandro.peliculas.models.entity.Usuario;

@Service
public class UsuarioService implements UserDetailsService {

	@Autowired
	private IUsuarioDao IUsuarioDao;

	@Transactional
	public void registrar(MultipartFile archivo, String nombre, String apellido, String mail, String clave,
			String clave2) throws ErrorService {

		validar(nombre, apellido, mail, clave, clave2);

		Usuario usuario = new Usuario();
		usuario.setNombre(nombre);
		usuario.setApellido(apellido);
		usuario.setMail(mail);

		String encriptada = new BCryptPasswordEncoder().encode(clave);
		usuario.setClave(encriptada);
		usuario.setAlta(new Date());

		IUsuarioDao.save(usuario);

	}

	@Transactional
	public void modificar(MultipartFile archivo, String id, String nombre, String apellido, String mail, String clave,
			String clave2) throws ErrorService {

		validar(nombre, apellido, mail, clave, clave2);

		Optional<Usuario> respuesta = IUsuarioDao.findById(id);
		if (respuesta.isPresent()) {

			Usuario usuario = respuesta.get();
			usuario.setApellido(apellido);
			usuario.setNombre(nombre);
			usuario.setMail(mail);
			String encriptada = new BCryptPasswordEncoder().encode(clave);
			usuario.setClave(encriptada);

			IUsuarioDao.save(usuario);
		} else {
			throw new ErrorService("no se ha encontrado el usuario seleccionado");

		}

	}

	@Transactional
	public void deshabilitar(String id) throws ErrorService {

		Optional<Usuario> respuesta = IUsuarioDao.findById(id);
		if (respuesta.isPresent()) {
			Usuario usuario = respuesta.get();
			usuario.setBaja(new Date());
			IUsuarioDao.save(usuario);
		} else {
			throw new ErrorService("no se pudo encontrar el ususario seleccionado");

		}

	}

	@Transactional
	public void habilitar(String id) throws ErrorService {

		Optional<Usuario> respuesta = IUsuarioDao.findById(id);
		if (respuesta.isPresent()) {
			Usuario usuario = respuesta.get();
			usuario.setBaja(null);
			IUsuarioDao.save(usuario);
		} else {
			throw new ErrorService("no se pudo encontrar el ususario seleccionado");

		}

	}
		
	
	public void validar(String nombre, String apellido, String mail, String clave, String clave2) throws ErrorService {

		if (nombre == null || nombre.isEmpty()) {
			throw new ErrorService("El nombre del usuario no puede ser nulo");
		}

		if (apellido == null || apellido.isEmpty()) {
			throw new ErrorService("El apellido del usuario no puede ser nulo");
		}

		if (mail == null || mail.isEmpty()) {
			throw new ErrorService("El mail no puede ser nulo");
		}

		if (clave == null || clave.isEmpty() || clave.length() <= 6) {
			throw new ErrorService("La clave del usuario no puede ser nula y tiene que tener mas de seis digitos");
		}
		if (!clave.equals(clave2)) {
			throw new ErrorService("Las claves deben ser iguales");
		}

	}

	@Override
	public UserDetails loadUserByUsername(String mail) throws UsernameNotFoundException {

		Usuario usuario = IUsuarioDao.buscarPorMail(mail);
		if (usuario != null) {
			List<GrantedAuthority> permisos = new ArrayList<>();

			GrantedAuthority p1 = new SimpleGrantedAuthority("ROLE_USUARIO_REGISTRADO");
			permisos.add(p1);

			ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();

			HttpSession session = attr.getRequest().getSession(true);
			session.setAttribute("usuariosession", usuario);

			User user = new User(usuario.getMail(), usuario.getClave(), permisos);
			return user;

		} else {
			return null;
		}

	}
	
	@Transactional(readOnly = true)
	public Usuario buscarPorId(String id) throws ErrorService {
		
		Optional<Usuario> respuesta = IUsuarioDao.findById(id);
		
		if (respuesta.isPresent()) {
			
			Usuario usuario = respuesta.get();
			return usuario;
		} else {
			throw new ErrorService("no se pudo encontrar el usuario seleccionado");
		}
		
	}
	
	
	
	
	@Transactional(readOnly = true)
	public List<Usuario> todosLosUsuarios(){
		return IUsuarioDao.findAll();
		
	}

}
