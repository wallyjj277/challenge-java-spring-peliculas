package com.alejandro.peliculas.models.service;

import java.util.List;


import com.alejandro.peliculas.models.entity.Personaje;

public interface IPersonajeService {
	List <Personaje> findAll();
	
	Personaje createPersonaje(Personaje personaje);

	Personaje updatePersonaje(Long idPersonaje, Personaje personaje);

	Personaje eliminarPersonaje(Personaje personaje);


}
