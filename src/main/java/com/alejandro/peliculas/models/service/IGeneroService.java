package com.alejandro.peliculas.models.service;

import java.util.List;

import com.alejandro.peliculas.models.entity.Genero;

public interface IGeneroService {
	
	
	

	List<Genero> findAll();

	Genero createGenero(Genero genero);

	Genero updateGenero(Long idGenero, Genero genero);

	 void eliminarGenero(Long idGenero);

}
