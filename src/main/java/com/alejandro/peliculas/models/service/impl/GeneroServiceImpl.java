package com.alejandro.peliculas.models.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import com.alejandro.peliculas.models.dao.IGeneroDao;
import com.alejandro.peliculas.models.entity.Genero;
import com.alejandro.peliculas.models.service.IGeneroService;
@Service
public class GeneroServiceImpl implements IGeneroService{
	
	@Autowired
	IGeneroDao iGeneroDao;

	@Override
	public List<Genero> findAll() {
		
		return iGeneroDao.findAll();
	}

	@Override
	public Genero createGenero(Genero genero) {
							
		return iGeneroDao.save(genero);
	}

	@Override
	public Genero updateGenero(Long idGenero, Genero genero) {
		
		Optional<Genero> optionalGenero= iGeneroDao.findById(idGenero);
		
		if(!optionalGenero.isPresent()) 
			return null;
		
		genero.setIdGenero(idGenero);
		
		
		
		return iGeneroDao.save(genero);
	}

	@Override
	public void eliminarGenero(Long idGenero) {
		
		iGeneroDao.deleteById(idGenero);
	}
}




