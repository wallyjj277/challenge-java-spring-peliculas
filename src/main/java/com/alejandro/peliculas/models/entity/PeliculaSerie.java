package com.alejandro.peliculas.models.entity;


	
	import javax.persistence.*;
	import java.time.LocalDateTime;
	import java.util.HashSet;
	import java.util.Set;

	@Entity
	@Table(name = "PeliculaSeries")
	public class PeliculaSerie {

	    @Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    private Long idPeliculaSerie;
	    private String imagen;
	    private String titulo;
	    private LocalDateTime fechaCreacion;
	    private int calificacion;

	    @ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, mappedBy = "peliculaSeries")
	    private Set<Personaje> personajes = new HashSet<>();

	    @ManyToOne
	    @JoinColumn(nullable = false, name = "genero")
	    private Genero genero;

	    public Long getIdPeliculaSerie() {
	        return idPeliculaSerie;
	    }

	    public void setIdPeliculaSerie(Long idPeliculaSerie) {
	        this.idPeliculaSerie = idPeliculaSerie;
	    }

	    public String getImagen() {
	        return imagen;
	    }

	    public void setImagen(String imagen) {
	        this.imagen = imagen;
	    }

	    public String getTitulo() {
	        return titulo;
	    }

	    public void setTitulo(String titulo) {
	        this.titulo = titulo;
	    }

	    public LocalDateTime getFechaCreacion() {
	        return fechaCreacion;
	    }

	    public void setFechaCreacion(LocalDateTime fechaCreacion) {
	        this.fechaCreacion = fechaCreacion;
	    }

	    public int getCalificacion() {
	        return calificacion;
	    }

	    public void setCalificacion(int calificacion) {
	        this.calificacion = calificacion;
	    }

	    public Set<Personaje> getPersonajes() {
	        return personajes;
	    }

	    public void setPersonajes(Set<Personaje> personajes) {
	        this.personajes = personajes;
	    }

	    public Genero getGenero() {
	        return genero;
	    }

	    public void setGenero(Genero genero) {
	        this.genero = genero;
	    }
	}


