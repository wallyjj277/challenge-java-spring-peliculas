package com.alejandro.peliculas.models.entity;

import java.util.Set;

import javax.persistence.*;


	@Entity
	@Table	(name = "Generos")
	public class Genero {
	    @Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    private Long idGenero;
	    
	    private String nombre;
	    private String imagen;
	    

	    @OneToMany(mappedBy = "genero")
	    private Set<PeliculaSerie> peliculaSeries;
	    
	    
	    

	    public Genero() {
			super();
		}

		public Genero( String nombre, String imagen, Set<PeliculaSerie> peliculaSeries) {
			super();
			
			this.nombre = nombre;
			this.imagen = imagen;
			this.peliculaSeries = peliculaSeries;
		}

		public Long getIdGenero() {
	        return idGenero;
	    }

	    public void setIdGenero(Long idGenero) {
	        this.idGenero = idGenero;
	    }

	    public String getNombre() {
	        return nombre;
	    }

	    public void setNombre(String nombre) {
	        this.nombre = nombre;
	    }

	    public String getImagen() {
	        return imagen;
	    }

	    public void setImagen(String imagen) {
	        this.imagen = imagen;
	    }

	    public Set<PeliculaSerie> getPeliculaSeries() {
	        return peliculaSeries;
	    }

	    public void setPeliculaSeries(Set<PeliculaSerie> peliculaSeries) {
	        this.peliculaSeries = peliculaSeries;
	    }


	}
	


