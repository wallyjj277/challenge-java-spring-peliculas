package com.alejandro.peliculas.models.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.alejandro.peliculas.models.entity.Genero;

public interface IGeneroDao extends JpaRepository<Genero, Long> {

}
