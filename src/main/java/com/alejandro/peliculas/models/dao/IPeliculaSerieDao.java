package com.alejandro.peliculas.models.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.alejandro.peliculas.models.entity.PeliculaSerie;

public interface IPeliculaSerieDao extends JpaRepository<PeliculaSerie, Long> {

}
