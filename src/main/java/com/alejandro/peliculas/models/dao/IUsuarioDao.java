package com.alejandro.peliculas.models.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.alejandro.peliculas.models.entity.Usuario;

public interface IUsuarioDao extends JpaRepository<Usuario, String> {

	@Query("SELECT c FROM Usuario WHERE c.mail = :mail ")
	public Usuario buscarPorMail(@Param("mail") String mail);

}
