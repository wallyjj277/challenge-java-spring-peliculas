package com.alejandro.peliculas.models.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.alejandro.peliculas.models.entity.Personaje;

public interface IPersonajeDao extends JpaRepository<Personaje, Long> {
	
	@Query(value= "SELECT * FROM PERSONAJES WHERE NOMBRE= :nombre and EDAD >= :edad", nativeQuery = true)
	List <Personaje> findByNameAndAge(@Param("nombre") String nombre, @Param("edad") int edad); 

}
