package com.alejandro.peliculas;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

import com.alejandro.peliculas.models.service.UsuarioService;


public class ServletInitializer  extends SpringBootServletInitializer{
	@Autowired
	private UsuarioService usuarioService;
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(PeliculasApplication.class);
	}

}
